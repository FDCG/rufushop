import { prods } from './prods';

export const getFetch = new Promise((res, rej) => {
    try {
        setTimeout(() => {
            res(prods)
        }, 2000)
    }catch (e) { rej(`"Error 404: ${e}"`) }
})

export const getById = (id) => {

    return new Promise((res, rej) => {

        try {
            setTimeout(() => {
                let prod = prods.find(prod => prod.id === id)
                res(prod)
            }, 2000)
        } catch (e) { rej(`"Error 404: ${e}"`) }


    }
    )
}

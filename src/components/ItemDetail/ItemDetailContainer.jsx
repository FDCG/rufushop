import React, { useEffect, useState } from 'react'
import { ItemDetail } from './ItemDetail';
import { useParams } from 'react-router-dom'
import { BallTriangle } from 'react-loader-spinner'
import { getProductById } from '../../dao/daoProducts'

export const ItemDetailContainer = () => {

    const [prod, setProd] = useState({})

    const [loading, setLoading] = useState(true)

    const { id } = useParams()

    useEffect(() => {

        getProductById(id)
            .then(res => setProd(res))
            .catch(err => console.log(err))
            .finally(() => setLoading(false))


    }, [id])



    return <div className="container mt-4">
        {loading
            ? <BallTriangle
                className="container center"
                color="#0f0101bc"
                height={200}
                width={200} />
            : <ItemDetail prod={prod} />}
    </div>;
};


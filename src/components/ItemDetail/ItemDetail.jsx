import React, { useState } from "react";
import { useCartContext } from '../../context/CartContext'
import { Link, useNavigate } from 'react-router-dom';
import { ItemCount } from "../Item/ItemCount";

export const ItemDetail = ({ prod }) => {

  const navigate = useNavigate();

  const [counter, setCounter] = useState(0);

  const { addProd, removeProd } = useCartContext()

  const onAdd = (cant) => {
    addProd({ ...prod, cantidad: cant })
    setCounter(cant)
  }
  /* PROVISORIO: Proof removeProd() */
  const onRemove = (prod) => {
    removeProd(prod.id)
    setCounter(0)
  }


  return <div className="d-flex flex-column">

    <div className="card mb-3" /* style={"max-width: 540px;"} */>

      <div className="row g-0">

        <div className="col-md-4">
          <img src={prod.foto} className="img-fluid rounded-start" alt='iamgen' />
        </div>

        <div className="col-md-8">

          <h1 className="card-header">{prod.nombre}</h1>

          <div className="card-body">
            <p className="card-text">{prod.detalle}</p>
            <h3 className="card-title">{`$ ${prod.precio}`}</h3>
            <p className="card-text"><small className="text-muted">{`Unidades Disponibles: ${(prod.stock > 0) ? prod.stock : 'Sin stock'}`}</small></p>
          </div>
          {counter === 0
            ? <ItemCount
              onAdd={onAdd}
              stock={prod.stock}
              initial={prod.stock === 0 ? 0 : 1} />
            :
            <><Link to='/cart'>
              <button className="btn btn-success m-3">Terminar mi compra!</button>
            </Link>
              {/* PROVISORIO: Proof onRemove() */}
              <button onClick={() => onRemove(prod)} className="btn btn-danger m-3">Me Arrepenti :( Eliminar del Carrito</button>
            </>
          }


          <div className="card-footer">
            <button
              className="btn btn-primary icon-left m-2"
              onClick={() => navigate(-1)}>
              {counter === 0
                ? 'Volver'
                : 'Seguir Comprando :)'}
            </button>



          </div>

        </div>


      </div>
    </div>
  </div>
};

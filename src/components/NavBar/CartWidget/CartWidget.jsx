import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useCartContext } from '../../../context/CartContext'

export const CartWidget = () => {

    const [quantity, setQuantity] = useState(0);

    const { cartList, totalQuantity } = useCartContext()

    useEffect(() => {
        setQuantity(totalQuantity)
    }, [cartList, totalQuantity])

    return (
        <div>
            <Link to="/cart">
                <button className="btn btn-dark">
                    <img src="/shopping-cart (2).png" alt="cart" />
                    {(quantity > 0) ?
                        <span className="badge bg-danger m-1">{quantity}</span>
                        : <></>}
                </button>
            </Link>
        </div>
    );
}

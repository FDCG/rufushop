import React from 'react'
import { CartWidget } from '../CartWidget/CartWidget';
import './navbar.css'
import { NavLink } from 'react-router-dom'




export const NavBar = ({ categories }) => {


    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container">
                    <NavLink to='/' className="navbar-brand">Rufushop</NavLink>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNav">
                        <ul className="navbar-nav">

                            {categories.map((category, index) =>
                                <li className="nav-item active" key={index} >
                                    <NavLink
                                        to={`/category/${category.toLowerCase()}`}
                                        className="nav-link"
                                    >
                                        {category}
                                    </NavLink>
                                </li>)}

                        </ul>
                    </div>
                    <CartWidget className="widget" />
                </div>
            </nav>

        </div>

    );
}
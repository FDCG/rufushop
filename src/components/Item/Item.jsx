
import React from 'react'
import { Link } from 'react-router-dom'
import '../../css/images.css'


export const Item = ({ prod }) => {


    return <div className="container">
        <div className="card h-100">
            <Link to={`/detail/${prod.id}`}>
                <img id="imgs" src={prod.foto} className="card-img-top p-4" alt="foto" width="300px" height="250px" />
            </Link>
            <div className="card-body">

                <h5 className="card-header">{`${prod.nombre}`} </h5>
                <h6 className="card-header">{`${prod.categoria}`} </h6>
                <p></p>
                <h3 className="card-text">{`$ ${prod.precio}`}</h3>

            </div>
            <div className="card-footer">
                <Link to={`/detail/${prod.id}`}>
                    <button className="btn btn-primary">Detalle</button>
                </Link>

            </div>
        </div>

    </div>
};

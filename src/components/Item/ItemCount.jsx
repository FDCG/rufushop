import React, { useState } from 'react'
import '../../css/itemcard.css'


export const ItemCount = ({ stock, initial, onAdd }) => {

    const [counter, setCounter] = useState(initial)

    const handleIncrease = () => {
        if (counter < stock) {
            setCounter(counter + 1)
        }
    }

    const handleDecrease = () => {
        if (counter > initial) {
            setCounter(counter - 1)
        }
    }

    const add = () => {
        onAdd(counter)
    }

    return (
        <div className="container m-2">

            <div className="input-group d-flex justify-content-center">

                <button className="btn btn-outline-danger" onClick={handleDecrease}> - </button>
                <input disabled={true} style={{ textAlign: 'center', width: '40px' }} value={counter}></input>
                <button className="btn btn-outline-success" onClick={handleIncrease}> + </button>
                <button
                    className="btn btn-primary"
                    onClick={add}
                    disabled={stock === 0}
                >
                    Agregar
                </button>
            </div>

        </div>

    )
}

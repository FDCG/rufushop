import React, { useEffect, useState } from 'react'
import { ItemList } from './ItemList'
import { useParams } from 'react-router-dom'
import { BallTriangle } from 'react-loader-spinner'
import { getProducts } from '../../dao/daoProducts'


export const ItemListContainer = () => {

    const [prods, setProds] = useState([])

    const [loading, setLoading] = useState(true)

    const { idCategory } = useParams()


    useEffect(() => {

        setLoading(true)
        getProducts(idCategory)
            .then(res => setProds(res))
            .catch(err => console.log(err))
            .finally(setLoading(false))

    }, [idCategory])


    return (
        <div className="container mt-4">

            {loading
                ? <BallTriangle
                    className="container"
                    color="#0f0101bc"
                    height={200}
                    width={200} />
                : <ItemList prods={prods} />}

        </div>
    )
}

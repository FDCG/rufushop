import React from "react"
import { Item } from "../Item/Item";


export const ItemList = ( { prods } ) => {
    
    return <div className="row row-cols-3 row-cols-md-3 g-4 mt-2">

        {prods.map(prod =>
            <Item key={prod.id} prod={prod} />
        )}

    </div>;
};

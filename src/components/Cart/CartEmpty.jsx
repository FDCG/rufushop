import React from 'react'
import { NavLink } from 'react-router-dom'

export const CartEmpty = () => {
    return (
        <div className="card">
            <div className="card-body">
                <span>No tenés articulos en tu carrito de compras</span>
            </div>
            <div className="card-footer">
                <NavLink to='/' className="navbar-brand"><button className="btn btn-warning btn-block btn-lg">Ir a la Tienda</button></NavLink>
            </div>
        </div>
    )
}

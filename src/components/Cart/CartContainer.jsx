import React, { useEffect, useState } from 'react';
import { useCartContext } from '../../context/CartContext';
import { Cart } from './Cart';
import { CartEmpty } from './CartEmpty';

export const CartContainer = () => {

  const [total, setTotal] = useState(0)

  const { cartList, removeProd, clear, totalPrice } = useCartContext()

  useEffect(() => {
     setTotal(totalPrice)
  }, [cartList, totalPrice])


  return <div className="cart-container">
    {cartList.length === 0
      ? <CartEmpty />
      : <Cart 
          cartList={cartList} 
          removeProd={removeProd} 
          total={total}
          clear={clear}
          
          ></Cart>
    }
  </div>;
};

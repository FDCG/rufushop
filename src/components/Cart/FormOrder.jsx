import React from 'react'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export const FormOrder = ({ onDoOrder, onClearCart }) => {

    const wait = 3000

    const doOrder = async (e) => {
        e.preventDefault();
        let inNombre = e.target.nombre.value.toString();
        let inTelefono = e.target.telefono.value.toString();
        let inEmail = e.target.email.value.toString();
        if (inNombre === "") { toast.error("Debe completar Nombre y Apellido...") }
        else if (inTelefono === "") { toast.error("Debe completar Telefono...") }
        else if (inEmail === "") { toast.error("Debe completar Email...") }
        else {


            await add(e, inNombre, inTelefono, inEmail)
                .then((res) => {
                    toast.success(`Order realizada correctamente. Número: ${res}`)
                        .then(setTimeout(() => {
                            cleanCart()
                        }, wait))
                })
                .catch((err) => toast.error(err))
        }
    }

    const cleanCart = () => {
        onClearCart()
    }

    const add = (e, inNombre, inTelefono, inEmail) => new Promise((res, rej) => {
        try {
            let rlt = onDoOrder(e, inNombre, inTelefono, inEmail)
            res(rlt)
        }
        catch (err) {
            rej(err)
        }
    })


    return (
        <div >
            <ToastContainer position="bottom-left"
                autoClose={wait}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover />

            <div className="card bg-primary text-white rounded-3">
                <div className="card-body">
                    <h3>Check Out</h3>
                    <form className="mt-4" onSubmit={doOrder}>

                        <div className="form-outline form-white mb-4">
                            <input type="text" name="nombre" className="form-control form-control-lg" size="17"
                                placeholder="Juan Pérez" />
                            <label className="form-label" >Nombre y Apellido</label>
                        </div>

                        <div className="form-outline form-white mb-4">
                            <input type="text" name="telefono" className="form-control form-control-lg" size="17"
                                placeholder="Por ejemplo: 1132783525" min="9" max="10" />
                            <label className="form-label" >Telefono</label>
                        </div>

                        <div className="form-outline form-white mb-4">
                            <input type="text" name="email" className="form-control form-control-lg" size="17"
                                placeholder="suemail@dominio.com" />
                            <label className="form-label" >Email</label>
                        </div>
                        <button type="submit" className="btn btn-warning btn-lg col-md-4 offset-lg-0 m-1">Confirmar Compra</button>

                    </form>



                    <button type="button" onClick={cleanCart} className="btn btn-secondary btn-lg col-md-4 offset-lg-0 m-1">Vaciar Carrito</button>
                </div>



            </div>

        </div >
    )
}

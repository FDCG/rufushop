import { React } from 'react';
import { addNewOrder, updateProd } from '../../dao/daoProducts'
import { FormOrder } from './FormOrder';

export const Cart = ({ cartList, removeProd, clear, total }) => {

    const deleteProduct = (id) => {
        removeProd(id);
    }


    const cleanCart = () => { clear() }


    const doOrder = async (e, nombre, telefono, mail) => {
        e.preventDefault();

        let orders = {}

        orders.comprador = { nombre: nombre, telefono: telefono, mail: mail }
        orders.total = total
        orders.items = cartList.map(cartItem => {
            const id = cartItem.id
            const nombre = cartItem.nombre
            const precio = cartItem.precio * cartItem.cantidad
            const cantidad = cartItem.cantidad

            return {
                id,
                nombre,
                precio,
                cantidad
            }
        })


        const result = await addNewOrder(orders)
            .then((res) => { 
                updateProd(cartList)
                return res 
            })
            .catch(err => console.log(err))

        return result

    }


    return (
        <div>
            <div className="container h-100 py-5">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-10">

                        <div className="d-flex justify-content-between align-items-center mb-4">
                            <h3 className="fw-normal mb-0 text-black">CARRITO DE COMPRAS</h3>
                        </div>
                        {cartList.map(cl =>
                            <div className="card rounded-3 mb-4" key={cl.id}>
                                <div className="card-body p-4">
                                    <div className="row d-flex justify-content-between align-items-center">
                                        <div className="col-md-2 col-lg-2 col-xl-2">
                                            <img src={cl.foto}
                                                className="img-fluid rounded-3" alt={cl.nombre} />
                                        </div>
                                        <div className="col-md-4 col-lg-3 col-xl-3">
                                            <p name="nombre" className="lead fw-normal mb-2">{cl.nombre}</p>
                                            <p><span className="text-muted">Categoria: </span>{cl.categoria.toUpperCase()}</p>
                                        </div>
                                        <div className="col-md-2 col-lg-2 col-xl-2 d-flex">

                                            <input min="0" name="cantidad" value={cl.cantidad} disabled
                                                className="form-control form-control-sm" />

                                        </div>
                                        <div className="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                                            <h5 className="mb-0">${(cl.precio * cl.cantidad)}</h5>
                                        </div>
                                        <div className="col-md-1 col-lg-1 col-xl-1">
                                            <button onClick={() => deleteProduct(cl.id)} className="btn btn-danger"><i className="fas fa-trash fa-lg"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )

                        }
                        <div className="card">
                            <div className="card-body">
                                <div className="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                                    <h5 className="mb-0">TOTAL: ${total}</h5>
                                </div>
                            </div>
                        </div>
                        <FormOrder
                            onDoOrder={doOrder}
                            onClearCart={cleanCart}

                        ></FormOrder>


                    </div>
                </div>
            </div>

        </div>

    );
};


import { CartContextProvider } from './context/CartContext'
import { NavBar } from './components/NavBar/NavBar/NavBar'
import { ItemListContainer } from './components/ItemList/ItemListContainer';
import { ItemDetailContainer } from './components/ItemDetail/ItemDetailContainer';
import { CartContainer } from './components/Cart/CartContainer';
import './App.css';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { categories } from './helpers/categories'

function App() {


    return (
        <CartContextProvider>
            <BrowserRouter>
                <div className="App" >
                    <NavBar categories={categories} />
                    <Routes>
                        <Route exact path="/" element={<ItemListContainer />} />
                        <Route exact path="/category/:idCategory" element={<ItemListContainer />} />
                        <Route exact path="/detail/:id" element={<ItemDetailContainer />} />
                        <Route exact path="/cart" element={<CartContainer />} />
                    </Routes>
                </div>
            </BrowserRouter>
        </CartContextProvider>
    );
}

export default App;
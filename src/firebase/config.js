// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB_9Ipu2x7I4XRIGWjzbDJUn2eF4ViwF60",
  authDomain: "rufuseshop.firebaseapp.com",
  projectId: "rufuseshop",
  storageBucket: "rufuseshop.appspot.com",
  messagingSenderId: "697580701839",
  appId: "1:697580701839:web:8502fd11ad0b978535c8fa"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


export default function getFirestoreApp(){
    return app
}

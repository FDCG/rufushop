import { collection, doc, getDoc, getDocs, getFirestore, where, query, addDoc, writeBatch, documentId } from "firebase/firestore"

const getProducts = async (idCategoria) => {

    const db = getFirestore();

    let prodCollection = collection(db, 'productos')

    if (idCategoria) { prodCollection = query(prodCollection, where('categoria', '==', idCategoria)) }

    const result = await getDocs(prodCollection)
        .then((res) => res.docs.map(doc => ({ id: doc.id, ...doc.data() })))
        .catch((err) => console.error(err))
        .finally()

    return result

}

const getProductById = async (idProd) => {

    const db = getFirestore()

    const prodCollection = doc(db, 'productos', idProd)

    const result = await getDoc(prodCollection)
        .then(resp => ({ id: resp.id, ...resp.data() }))
        .catch(err => console.log(err))
        .finally()

    return result
}

const addNewOrder = async (order) => {
    const db = getFirestore()

    const ordersCollection = collection(db, 'ordenes')

    const id = await addDoc(ordersCollection, order)
        .then((res) => res.id)
        .catch(err => console.log(err))
        .finally()

    return id

}

const updateProd = async (cartList) => {
    const db = getFirestore()
    const queryCollection = collection(db, 'productos')


    const queryUpdateStock = query(
        queryCollection,
        where(documentId(), 'in', cartList.map(p => p.id))
    )

    const batch = writeBatch(db)

    await getDocs(queryUpdateStock)
        .then(resp => resp.docs.forEach(res => batch.update(res.ref, {
            stock: res.data().stock - cartList.find(p => p.id === res.id).cantidad
        })
        ))
        .catch(err => console.log(err))
        
    await batch.commit()

}

export { getProductById, getProducts, addNewOrder, updateProd }




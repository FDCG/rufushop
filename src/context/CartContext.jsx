import { createContext, useContext, useState } from 'react';

const CartContext = createContext([])

export const useCartContext = () => useContext(CartContext)

export const CartContextProvider = ({ children }) => {

    const [cartList, setCartList] = useState([]);

    const addProd = (producto) => {
        if (isInCart(producto.id)) {
            const cartListAdd = [...cartList]
            cartListAdd.forEach(p => {
                if (p.id === producto.id) {
                    p.cantidad += producto.cantidad
                }
            })
            setCartList(cartListAdd)
        } else { setCartList([...cartList, producto]) }
    }

    const removeProd = (id) => {
        if (isInCart(id)) {
            const cartListRemoved = cartList.filter(p => p.id !== id)
            setCartList(cartListRemoved)
        } else { throw new Error("El producto que intenta eliminar no está en Carrito") }
    }

    const clear = () => { setCartList([]) }

    const isInCart = (id) => {
        return cartList.find(p => p.id === id)
    }

    const totalPrice = () => { 
        return cartList.reduce((sum, p) => sum+=p.precio*p.cantidad, 0)
    }

    const totalQuantity = () => { 
        return cartList.reduce((sum, p) => sum+=p.cantidad, 0)
    }

    return (
        <CartContext.Provider 
        value={{ 
            addProd, 
            removeProd, 
            clear, 
            totalPrice, 
            totalQuantity,
            cartList }}>
            {children}
        </CartContext.Provider>
    )
}
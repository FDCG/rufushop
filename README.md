# **Proyecto: Rufushop en React. E-commerce**

Rufushop es una plataforma que te permite ver y comprar en linea diversos productos para tus mascotas.
Encontrarás alimentos, prendas, accesorios para perros y gatos de todas las edades.

## Ejecución

Para iniciarlo por consola  `npm start` en puerto 3000

## Modulos-Dependencias

Utiliza modulo **_'react-loader-spinner'_**  para renderizar la pantalla de carga
Utiliza modulo **_'react-toastify'_**  para lanzar toast al confirmar las ordenes



